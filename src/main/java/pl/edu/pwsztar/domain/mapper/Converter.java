package pl.edu.pwsztar.domain.mapper;

public interface Converter<F,T> {
    T convert(F from);
}
